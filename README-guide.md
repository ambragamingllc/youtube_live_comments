#Abaixo um tutorial de como configurar passo a passo

- Configuração do projeto
    - configure o setup do projeto/banco no arquivo .env
    - execute os comandos abaixo
        - composer install
        - npm install && npm run production
        - php artisan migrate
        
    - (etapa 2) adicione os campos abaixo no .env
        - GOOGLE_CLIENT_ID
        - GOOGLE_CLIENT_SECRET
        - GOOGLE_URL
        
    - Para conseguir autenticar com o google é necessário fazer algumas configurações
        - acesse https://console.developers.google.com/apis/dashboard
        - crie um novo projeto
        
        - acesse https://console.developers.google.com/apis/credentials?folder=&organizationId=
        - em "CRIAR CREDENCIAIS", selecione "Chave de API"
        - Novamente em "CRIAR CREDENCIAIS", selecione "ID DO CLIENTE OAUTH"
        - configure de acordo com o tipo de aplicação que irá utilizar (neste caso selecione "Aplicativo da Web")
        
        - Na opção "URIs de redirecionamento autorizados" você deverá informar as urls que servirão de callback para o google OAuth.
            - cadastre a url que irá efetuar o login socialite, setup da live e o envio da mensagem
                - Caso seu projeto esteja rodando localhost
                - auth socialite : http://127.0.0.1:8000/social-auth-callback       
                - setup de acesso e live : http://127.0.0.1:8000/setLiveChatId
                    - http://127.0.0.1:8000/index.php/setLiveChatId
                - envio da mensagem manual : http://127.0.0.1:8000/postMessage
                    - http://127.0.0.1:8000/index.php/postMessage 
                           
                                    
            - Na mesma tela, a direita você verá os campos
            - ID do Cliente
            - Chave secreta do cliente
            - Estes campos serão necessários na tela de setup do projeto, e também preencha os campos da etapa 2 com estes dados        
            **
    
   # Por último é necessáro ativar a API do Youtube
   - (que servirá como seu identificador nas requisições)
   - Clique em biblioteca (https://console.developers.google.com/apis/library?folder=&organizationId=&project=)      
   - Procure por : "Youtube"
   - Selecione "Youtube Data API V3"
   - Clique em "Ativar".
   
   #Pronto, agora o você já pode começar a utilizar o projeto!.
                 