<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    #return view('welcome');
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('social-auth-google', 'Auth\LoginSocialController@loginSocialGoogle')->name('social-auth-google');
Route::get('social-auth-callback', 'Auth\LoginSocialController@getAuthenticatedLoginSocial')->name('social-auth-callback');

Route::get('setLiveChatId', 'HomeController@setLiveChatId')->name('set-live-chat-id');
Route::get('sendMessage', 'HomeController@sendMessage')->name('send-message');
Route::get('postMessage', 'HomeController@postMessage')->name('post-message');

Route::get('cronJob', 'CronjobConfigController@index')->name('cron-job');
Route::get('cronJobStart', 'CronjobConfigController@start')->name('cron-job-start');