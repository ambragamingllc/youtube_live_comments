<?php

declare(strict_types=1);

namespace App\Services;

use App\Console\Commands\SendLiveChatMessage;
use App\GoogleOAuthHelper;
use App\Models\CronjobConfig;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class CronJobService
{
    public function create(array $data): CronJobConfig
    {

        return DB::transaction(function () use ($data) {

            $model = new CronJobConfig();
            $model->fill($data);

            $model->save();

            return $model;
        });
    }

    public function createOrUpdate(array $data): CronJobConfig
    {

        return DB::transaction(function () use ($data) {

            /*$model = new CronJobConfig();
            $model->fill($data);

            $model->save();

            return $model;*/

            return CronjobConfig::firstOrCreate(
                $data
            );
        });
    }

    public function find(int $id): ?CronJobConfig
    {
        return CronJobConfig::find($id);
    }

    public function update(array $data, CronJobConfig $model): CronJobConfig
    {

        $model->fill($data);
        $model->save();

        return $model;
    }

    public function delete(CronJobConfig $model): ?bool
    {
        $model->save();
        return $model->delete();
    }

    public function lists()
    {
        return CronJobConfig::all();
            #->pluck('name', 'id')
            #->toArray();

    }

    public function callCronJob()
    {
        $schedulle = new Schedule();

        $authReturn = (new GoogleOAuthHelper())->authenticate();

        $service = new CronJobService();
        $schedulle->call(function ($service, $authReturn) {
            $service->initCronJob($authReturn);
            #new SendLiveChatMessage();
        });
    }


    public function initCronJob($authReturn = null)
    {
        try {

            #$url = 'http://127.0.0.1:8000/index.php/postMessage';
            #$authReturn = (new GoogleOAuthHelper())->authenticate($url);

            if ($authReturn && count($authReturn)) {

                echo '------------------------------ Enviando mensagem ------------';

                $message = CronjobConfig::all()->first()->message;

                $response = (new GoogleOAuthHelper())->postMessage($authReturn['youtube_instance'],
                    [
                        'live_chat_id' => session()->get('live_chat_id'),
                        'message' => $message,
                    ]);

                echo '------------------------------ Mensagem enviada! ------------';

            } else {

                echo '------------------------------ Mensagem não enviada! ------------';
            }

        } catch (\Exception $e) {
            echo '------------------------------ Erro Exception ----------------';
            echo $e->getMessage();
            echo '------------------------------------------------------------';
        }
    }
}
