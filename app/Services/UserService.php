<?php

declare(strict_types=1);

namespace App\Services;

use App\GoogleOAuthHelper;
use App\Models\SocialAccount;
use App\Models\User;
use Google_Service_YouTube;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class UserService
{

    public function create(array $data): User
    {

        return DB::transaction(function () use ($data) {

            $model = new User();
            $model->fill($data);

            if (!empty($data["password"])) {
                $model->password = Hash::make($data["password"]);
            }

            $model->save();

            return $model;
        });
    }

    public function find(int $id): ?User
    {
        return User::find($id);
    }

    /**
     * @param $token
     * @param $provider [facebook, google]
     * @return
     * @throws \ErrorException
     */
    public function getAuthSocialData($token, $provider) #: Socialite
    {
        if (!$token) {
            throw new \ErrorException('token não informado', 401);
        }

        $socialAuthUserDetails = Socialite::driver($provider)->userFromToken($token);

        if (!$socialAuthUserDetails) {
            throw new \ErrorException('Dados não encontrados', 404);
        }

        return $socialAuthUserDetails;
    }

    /**
     * @param $provider
     * @return User
     */
    public function findOrNewSocialUser($provider): User
    {
        $userSocialData = Socialite::driver($provider)->user();

        session_start();

        session([
            'token' => $userSocialData->token,
            'refreshToken' => $userSocialData->refreshToken,
            'expiresIn' => $userSocialData->expiresIn,
            'user_social_id' => $userSocialData->id,
            'code' => request()->get('code'),
        ]);

        $socialAccount = SocialAccount::where('provider_user_id', '=', $userSocialData->getId())->first();

        if ($socialAccount) {

            $user = $this->find((int)$socialAccount->user_id);

        } else {

            $user = User::where('email', '=', $userSocialData->getEmail())->first();

            if (!$user) {

                $user = $this->create([
                    'name' => $userSocialData->getName(),
                    'email' => $userSocialData->getEmail(),
                ]);
            }

            SocialAccount::create([
                'user_id' => $user->id,
                'provider_user_id' => $userSocialData->getId(),
                'provider' => $provider,
            ]);
        }

        return $user;
    }

    public function testAccesToken()
    {
        $url = "https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=" . request()->session()->get('token');
        $client = new Client();
        $request = $client->get($url);

        $response = $request->getBody()->getContents();

        dd($response);

        return $response;
    }

    /**
     * @param Google_Service_YouTube $youtube
     */
    public function setLiveChatId(Google_Service_YouTube $youtube)
    {
        $broadcastsResponse = $youtube->liveBroadcasts->listLiveBroadcasts(
            'snippet,id',
            [
                'mine' => 'true',
            ]);

        foreach ($broadcastsResponse['items'] as $key => $broadcastItem) {

            if ($broadcastItem->id == request()->session()->get('live_id')) {
                session(['live_chat_id' => $broadcastItem->snippet->liveChatId]);
            }
        }

        #return $broadcastsResponse;
    }

    /**
     * @param Google_Service_YouTube $youtube
     * @return \Google_Service_YouTube_LiveChatMessage
     */
    public function postMessage(Google_Service_YouTube $youtube)
    {
        return (new GoogleOAuthHelper())->postMessage($youtube,
            [
                'live_chat_id' => session()->get('live_chat_id'),
                'message' => session()->get('message'),
            ]);
    }


}
