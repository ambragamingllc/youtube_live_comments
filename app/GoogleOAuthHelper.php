<?php

namespace App;

use Google_Client;
use Google_Service_YouTube;
use Google_Service_YouTube_LiveChatMessage;
use Google_Service_YouTube_LiveChatMessageSnippet;
use Google_Service_YouTube_LiveChatTextMessageDetails;

class GoogleOAuthHelper
{
    public function __construct()
    {
        #session_start();
    }

    public function authenticate($urlRedirect = '')
    {
        $client = new Google_Client();

        $client->setClientId(session()->get('oauth_client_id'));
        $client->setClientSecret(session()->get('oauth_client_secret'));

        $client->setScopes('https://www.googleapis.com/auth/youtube');

        #$client->setScopes('https://www.googleapis.com/auth/youtube.readonly');
        $client->setAccessType('offline');

        $urlRedirect = ($urlRedirect) ? $urlRedirect : 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];

        $redirect = filter_var($urlRedirect,
            FILTER_SANITIZE_URL);
        $client->setRedirectUri($redirect);

        // Define an object that will be used to make all API requests.
        $youtube = new Google_Service_YouTube($client);

        // Check if an auth token exists for the required scopes
        $tokenSessionKey = 'token-' . $client->prepareScopes();
        if (isset($_GET['code'])) {
            if (strval($_SESSION['state']) !== strval($_GET['state'])) {
                die('The session state did not match.');
            }

            $client->authenticate($_GET['code']);
            $_SESSION[$tokenSessionKey] = $client->getAccessToken();
            header('Location: ' . $redirect);
            exit();
        }

        if (isset($_SESSION[$tokenSessionKey])) {
            $client->setAccessToken($_SESSION[$tokenSessionKey]);
        }

        // Check to ensure that the access token was successfully acquired.
        if ($client->getAccessToken()) {

            $_SESSION[$tokenSessionKey] = $client->getAccessToken();

            return [
                'success' => true,
                'youtube_instance' => $youtube,
                'client_instance' => $client,
            ];

        } else {
            // If the user hasn't authorized the app, initiate the OAuth flow
            $state = mt_rand();
            $client->setState($state);
            $_SESSION['state'] = $state;

            $authUrl = $client->createAuthUrl();
            header('Location: ' . $authUrl);
            exit();
        }
    }

    /**
     * @param Google_Service_YouTube $youtube
     * @param array $data
     * @return Google_Service_YouTube_LiveChatMessage
     */
    public function postMessage(Google_Service_YouTube $youtube, array $data)
    {
        // Execute an API request that lists broadcasts owned by the user who
        // authorized the request.
        // Define the $liveChatMessage object, which will be uploaded as the request body.
        $liveChatMessage = new Google_Service_YouTube_LiveChatMessage();

        // Add 'snippet' object to the $liveChatMessage object.
        $liveChatMessageSnippet = new Google_Service_YouTube_LiveChatMessageSnippet();

        $liveChatMessageSnippet->setLiveChatId($data['live_chat_id']);

        $liveChatTextMessageDetails = new Google_Service_YouTube_LiveChatTextMessageDetails();

        $liveChatTextMessageDetails->setMessageText($data['message']);

        $liveChatMessageSnippet->setTextMessageDetails($liveChatTextMessageDetails);
        $liveChatMessageSnippet->setType('textMessageEvent');
        $liveChatMessage->setSnippet($liveChatMessageSnippet);

        return $youtube->liveChatMessages->insert('snippet', $liveChatMessage);
    }

}