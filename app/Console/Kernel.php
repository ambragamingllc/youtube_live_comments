<?php

namespace App\Console;

use App\Services\CronJobService;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        Commands\SendLiveChatMessage::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        $cronJobConfig = (new CronJobService())->lists()->first();

        $schedule->command('live_chat_message:send')->everyMinute();

        #$schedule->command('live_chat_message:send');

        /*if ($cronJobConfig) {

            switch ($cronJobConfig->time_interval) {
                case 'S':
                    #$schedule->command('live_chat_message:send')->cron($cronJobConfig->loop_interval * 1);
                    $schedule->command('live_chat_message:send')->everyMinute();
                    break;

                case 'M':
                    #$schedule->command('live_chat_message:send')->cron($cronJobConfig->loop_interval * 60);
                    $schedule->command('live_chat_message:send')->everyMinute();
                    break;

                case 'H':
                    #$schedule->command('live_chat_message:send')->cron($cronJobConfig->loop_interval * 60 * 60);
                    $schedule->command('live_chat_message:send')->everyMinute();
                    break;
            }
        }*/
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
