<?php

namespace App\Console\Commands;

use App\GoogleOAuthHelper;
use App\Services\CronJobService;
use App\Services\NotificationService;
use Illuminate\Console\Command;

class SendLiveChatMessage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'live_chat_message:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending message to live chat message';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param CronJobService $jobService
     */
    public function handle(CronJobService $jobService)
    {
        #$url = 'http://127.0.0.1:8000/index.php/postMessage';
        #$authReturn = (new GoogleOAuthHelper())->authenticate($url);
        #$jobService->initCronJob($authReturn);
        $jobService->initCronJob();
    }
}
