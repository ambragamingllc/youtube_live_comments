<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CronJobConfigRule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Validation rules that apply to the request.
     *v
     * @var array
     */
    protected static $rules = [
        'loop_interval' => 'required',
        'time_interval' => 'required',
        'message' => 'required|max:200',
    ];

    /**
     * Return default rules
     *
     * @return array
     */
    public static function rules()
    {
        return [
            'loop_interval' => self::$rules['loop_interval'],
            'time_interval' => self::$rules['time_interval'],
            'message' => self::$rules['message'],
        ];
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        #return 'The validation error message.';
        return [];
    }
}
