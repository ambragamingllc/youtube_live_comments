<?php

declare(strict_types=1);

namespace App\Rules;

class LiveRule
{

    /**
     * Validation rules that apply to the request.
     *v
     * @var array
     */
    protected static $rules = [
        'oauth_client_id' => 'required',
        'oauth_client_secret' => 'required',
        'live_id' => 'required',
        'message' => 'required|max:200',
    ];

    /**
     * Return default rules
     *
     * @return array
     */
    public static function rules()
    {
        return [
            'oauth_client_id' => self::$rules['oauth_client_id'],
            'oauth_client_secret' => self::$rules['oauth_client_secret'],
            'live_id' => self::$rules['live_id'],
            'message' => self::$rules['message'],
        ];
    }

    /**
     * Return default messages
     *
     * @return array
     */
    public static function messages()
    {

        return [];
    }
}
