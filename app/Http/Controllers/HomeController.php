<?php

namespace App\Http\Controllers;

use App\GoogleOAuthHelper;
use App\Http\Requests\LiveRequest;
use App\Http\Requests\MessageRequest;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class HomeController extends Controller
{
    /**
     * @var UserService
     */
    private $service;

    /**
     * Create a new controller instance.
     *
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->middleware('auth');
        session_start();
        $this->service = $userService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function setLiveChatId(Request $request)
    {
        try {

            $data = [
                'oauth_client_id' => (request()->get('oauth_client_id') ? request()->get('oauth_client_id') : session('oauth_client_id')),
                'oauth_client_secret' => (request()->get('oauth_client_secret') ? request()->get('oauth_client_secret') : session('oauth_client_secret')),
                'live_id' => (request()->get('live_id') ? request()->get('live_id') : session('live_id')),
            ];

            session($data);

            $request->request->add($data);

            $this->validate($request, (new LiveRequest())->rules());

            $authReturn = (new GoogleOAuthHelper())->authenticate();

            if ($authReturn && count($authReturn)) {

                $this->service->setLiveChatId($authReturn['youtube_instance']);
            }

            return redirect()->to('/postMessage');

        } catch (ValidationException $e) {

            return redirect()->to('home')->withInput()->withErrors($e->errors());

        } catch (\Exception $e) {

            return redirect()->to('home')->withInput()->withErrors($e->getMessage());
        }
    }

    public function postMessage()
    {
        return view('message');
    }

    public function sendMessage(Request $request)
    {
        try {

            $data = [
                'message' => (request()->get('message') ? request()->get('message') : session('message')),
            ];

            session($data);

            $request->request->add($data);

            $this->validate($request, (new MessageRequest())->rules());

            $authReturn = (new GoogleOAuthHelper())->authenticate();

            if ($authReturn && count($authReturn)) {

                $postMessage = $this->service->postMessage($authReturn['youtube_instance']);
            }

            return redirect(route('post-message'))->with('success', 'Enviado com sucesso.');

        } catch (ValidationException $e) {

            return redirect(route('post-message'))->withInput()->withErrors($e->errors());

        } catch (\Exception $e) {

            return redirect(route('post-message'))->withInput()->withErrors($e->getMessage());
        }
    }
}
