<?php

namespace App\Http\Controllers;

use App\Http\Requests\CronJobConfigRequest;
use App\Services\CronJobService;
use Illuminate\Http\Request;

class CronjobConfigController extends Controller
{
    private $service;

    /**
     * Create a new controller instance.
     *
     * @param CronJobService $jobService
     */
    public function __construct(CronJobService $jobService)
    {
        $this->middleware('auth');
        session_start();
        $this->service = $jobService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = $this->service->lists();
        return view('cronjob_config', ['data' => $data]);
    }

    public function start(CronJobConfigRequest $request)
    {
        try {

            $data = $this->service->createOrUpdate($request->all());

            $this->service->callCronJob();

            return redirect()->to('cronJob')->with('success', 'Enviado com sucesso.');

        } catch (\Exception $e) {

            dd($e->getMessage());
            #return redirect()->to('cronJob')->withInput()->withErrors($e->getMessage());
        }
    }
}
