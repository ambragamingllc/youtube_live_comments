<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\UserService;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class LoginSocialController extends Controller
{
    public function loginSocialGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function getAuthenticatedLoginSocial()
    {
        try {

            $provider = 'google';

            $userService = new UserService();

            $user = $userService->findOrNewSocialUser($provider);

            Auth::login($user);

            return redirect()->to('/home');

        } catch (\Exception $e) {

            #dd('Opa',$e->getMessage(), $e->getTraceAsString());
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
}
