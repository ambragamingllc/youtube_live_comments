<?php

declare(strict_types=1);

namespace App\Http\Requests;

use App\Rules\LiveRule;
use Illuminate\Foundation\Http\FormRequest;

class LiveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = LiveRule::rules();

        return [
            'live_id' => $rules['live_id'],
            'oauth_client_secret' => $rules['oauth_client_secret'],
            'oauth_client_id' => $rules['oauth_client_id'],
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return LiveRule::messages();
    }
}
