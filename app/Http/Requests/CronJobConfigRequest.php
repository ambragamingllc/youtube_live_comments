<?php

namespace App\Http\Requests;

use App\Rules\CronJobConfigRule;
use Illuminate\Foundation\Http\FormRequest;

class CronJobConfigRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = CronJobConfigRule::rules();
        return [
            'loop_interval' => $rules['loop_interval'],
            'time_interval' => $rules['time_interval'],
            'message' => $rules['message'],
        ];
    }

    public function messages()
    {
        return [];
    }
}
