<?php

return [

    'tracking_status' => [
        'PROGRESS' => 'Andamento',
        'COMPLETED' => 'Concluído'
    ],

    /*'frequency_alarms' => [
        'D1' => 'Dias',
        'D7' => 'Semanas',
        'D15' => 'Quinzenas',
        'M1' => 'Meses',
        'M2' => 'Bimestres',
        'M3' => 'Trimestres',
        'M6' => 'Semestres',
        'M12' => 'Anos',
        #'D0' => 'Outros'
    ],*/
    'frequency_time' => [
        'S' => 'Segundos',
        'M' => 'Minutos',
        'H' => 'Horas',
    ],
];
