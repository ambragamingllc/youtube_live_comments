@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Configuraçoes de acesso</div>

                    <div class="card-body">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger dev-mod">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form method="GET" action="{{ route('set-live-chat-id') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="oauth_client_id" class="col-md-4 col-form-label text-md-right">OAUTH CLIENT ID</label>

                                <div class="col-md-6">
                                    <input id="oauth_client_id" type="text"
                                           class="form-control @error('oauth_client_id') is-invalid @enderror" name="oauth_client_id"
                                           value="{{ old('oauth_client_id') }}" required autocomplete="oauth_client_id" autofocus>

                                    @error('oauth_client_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="oauth_client_secret" class="col-md-4 col-form-label text-md-right">CLIENT SECRET</label>

                                <div class="col-md-6">
                                    <input id="oauth_client_secret" type="oauth_client_secret"
                                           class="form-control @error('oauth_client_secret') is-invalid @enderror"
                                           name="oauth_client_secret"
                                           value="{{ old('oauth_client_secret') }}"
                                           required autocomplete="google token">

                                    @error('oauth_client_secret')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="live_id" class="col-md-4 col-form-label text-md-right">LIVE ID</label>

                                <div class="col-md-6">
                                    <input id="live_id" type="live_id"
                                           class="form-control @error('live_id') is-invalid @enderror"
                                           name="live_id"
                                           value="{{ old('live_id') }}"
                                           required autocomplete="google token">

                                    @error('live_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">

                                    <button type="submit" class="btn btn-primary">
                                        Avaçar
                                    </button>

                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
