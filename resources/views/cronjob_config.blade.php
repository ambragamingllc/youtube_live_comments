@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Configuraçoes de envio</div>

                    <div class="card-body">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger dev-mod">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @php
                            $data = ($data->count()) ? $data->first() : null;
                        @endphp

                        <form method="GET" action="{{ route('cron-job-start') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="message" class="col-md-4 col-form-label text-md-right">Comentário</label>

                                <div class="col-md-6">
                                    <textarea name="message" id="message"
                                              class="form-control @error('message') is-invalid @enderror" required
                                              autocomplete="message"
                                              autofocus
                                              maxlength="200">{{ old('message', ($data ? $data->message : old('message'))) }}</textarea>
                                    <small>máximo 200 caracteres</small>

                                    @error('message')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                            </div>

                            <div class="form-group row">
                                <label for="oauth_client_id" class="col-md-4 col-form-label text-md-right">Enviar a
                                    cada</label>

                                <div class="col-md-3">
                                    <input id="loop_interval" type="text"
                                           class="form-control @error('loop_interval') is-invalid @enderror"
                                           name="loop_interval"
                                           value="{{ old('loop_interval', ($data ? $data->loop_interval : old('loop_interval'))) }}"
                                           required autocomplete="loop_interval" autofocus>

                                    @error('loop_interval')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>


                                <div class="form-group col-md-3 @if ($errors->has('time_interval')) has-error @endif">
                                    <select class="form-control m-b" name="time_interval" id="time_interval">
                                        @foreach(config('enums.frequency_time') as $i => $v)
                                            <option
                                                    value="{{ $i }}" {{ old('time_interval', ($data ? $data->time_interval : old('time_interval'))) == $i ? 'selected' : null }}>
                                                {{ $v }}
                                            </option>
                                        @endforeach
                                    </select>
                                    {!! $errors->first('time_interval','<span class="help-block m-b-none">:message</span>') !!}
                                </div>

                                {{--@error('time_interval')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror--}}

                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">

                                    <button type="submit" class="btn btn-primary">
                                        Salvar
                                    </button>

                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
