@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Insira o comentário que deseja postar na live</div>

                    <div class="card-body">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger dev-mod">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form method="GET" action="{{ route('send-message') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="message" class="col-md-4 col-form-label text-md-right">Comentário</label>

                                <div class="col-md-6">
                                    <textarea name="message" id="message" class="form-control @error('message') is-invalid @enderror" required autocomplete="message" autofocus>{{ old('message') }}</textarea>
                                    <small>máximo 200 caracteres</small>

                                    @error('message')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">

                                    <button type="submit" class="btn btn-primary" id="btn_message">
                                        publicar comentario
                                    </button>

                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

        /*$().ready(function () {

            $("#btn_message").click(
                alert("clicado")
            );

        });*/

    </script>
@endsection